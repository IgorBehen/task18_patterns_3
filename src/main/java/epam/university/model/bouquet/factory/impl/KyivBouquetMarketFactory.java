package epam.university.model.bouquet.factory.impl;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import epam.university.model.bouquet.factory.BouquetMarketFactory;
import java.util.LinkedList;
import java.util.List;

public class KyivBouquetMarketFactory extends BouquetMarketFactory {

  public KyivBouquetMarketFactory() {
    availableBouquets = new LinkedList<>();
    availableBouquets.add(BouquetEnum.DREAMLAND);
    availableBouquets.add(BouquetEnum.FLORAL_FANTASY);
    availableBouquets.add(BouquetEnum.JUST_FOR_YOU);
    availableBouquets.add(BouquetEnum.DREAMLAND);
    availableBouquets.add(BouquetEnum.HARMONY);
    availableBouquets.add(BouquetEnum.SCENTED_DREAM);
  }

  public Bouquet getBouquet(BouquetEnum bouquetFromCatalog) {
    Bouquet bouquet = new Bouquet(bouquetFromCatalog.getFlowers());
    bouquet.setBouquetDecorationElements(bouquetFromCatalog.getDecorationElements());
    return bouquet;
  }

  public Bouquet getBouquet(List<Flower> flowerEnums) {
    return new Bouquet(flowerEnums);
  }

  public List<BouquetEnum> getAvailableBouquets() {
    return availableBouquets;
  }
}
