package epam.university.model.order;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.client.Client;
import epam.university.model.order.state.State;
import epam.university.model.order.state.impl.BouquetCreationState;

public class Order implements IOrder {

  private State state;
  private Bouquet bouquet;
  private final String NAME = "Order";
  private Client client;
  private double totalPrice;

  public Order() {
    state = new BouquetCreationState();
    totalPrice = 0;
  }

  public Order(Bouquet bouquet, Client client) {
    this.bouquet = bouquet;
    this.client = client;
    state = new BouquetCreationState();
    totalPrice = 0;
  }

  public Bouquet getBouquet() {
    return bouquet;
  }

  public void setState(State state) {
    this.state = state;
  }

  public void setBouquet(Bouquet bouquet) {
    this.bouquet = bouquet;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public void setTotalPrice(double totalPrice) {
    this.totalPrice = totalPrice;
  }

  public void bouquetCreation() {
    state.bouquetCreation(this);
  }

  public void bouquetDecoration() {
    state.bouquetDecoration(this);
  }

  public void registerOrder() {
    state.registerOrder(this);
  }

  public void deliveringOrder() {
    state.deliveringOrder(this);
  }

  public double getTotalPrice() {
    return totalPrice;
  }

  @Override
  public double getCost() {
    return bouquet.getPrice();
  }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public Client getClient() {
    return client;
  }

  @Override
  public String toString() {
    return "Order{" +
        "bouquet=" + bouquet +
        ", client=" + client +
        ", totalPrice=" + totalPrice +
        '}';
  }
}
