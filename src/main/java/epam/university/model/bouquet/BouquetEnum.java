package epam.university.model.bouquet;

import epam.university.model.bouquet.decoration.element.*;

import java.util.ArrayList;
import java.util.List;

public enum BouquetEnum {
    SPRING_BEAUTY(
            new ArrayList<Flower>() {{
                add(Flower.ROSE);
                add(Flower.GAZANIA);
                add(Flower.CORNFLOWER);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new RedTape());
                add(new WhitePackingPaper());
            }}),
    DREAMLAND(
            new ArrayList<Flower>() {{
                add(Flower.GERBERA);
                add(Flower.LILY);
                add(Flower.HORTENSIA);
                add(Flower.GLADIOLUS);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new GoldTape());
                add(new TransparentPackagingWrap());
            }}),
    SUMMER_GLORY(
            new ArrayList<Flower>() {{
                add(Flower.HORTENSIA);
                add(Flower.HORTENSIA);
                add(Flower.LAVENDER);
                add(Flower.NARCISSUS);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new RedTape());
                add(new BlackTape());
                add(new GreenPackingPaper());
            }}),
    HARMONY(
            new ArrayList<Flower>() {{
                add(Flower.PEONY);
                add(Flower.LAVENDER);
                add(Flower.PEONY);
                add(Flower.PEONY);
                add(Flower.LAVENDER);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new WhitePackingPaper());
                add(new PinkPackingPaper());
            }}
    ),
    PINK_PERFECTION(
            new ArrayList<Flower>() {{
                add(Flower.ROSE);
                add(Flower.CHRYSANTHEMUM);
                add(Flower.CHRYSANTHEMUM);
                add(Flower.GERBERA);
                add(Flower.ROSE);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new RedTape());
                add(new GoldTape());
                add(new PinkPackingPaper());
            }}
    ),
    FLORAL_FANTASY(
            new ArrayList<Flower>() {{
                add(Flower.CORNFLOWER);
                add(Flower.NARCISSUS);
                add(Flower.CORNFLOWER);
                add(Flower.NARCISSUS);
                add(Flower.CORNFLOWER);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new GreenPackingPaper());
                add(new WhitePackingPaper());
            }}
    ),
    MIDNIGHT_MAGIC(
            new ArrayList<Flower>() {{
                add(Flower.HORTENSIA);
                add(Flower.GLADIOLUS);
                add(Flower.PEONY);
                add(Flower.PEONY);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new GoldTape());
                add(new WhitePackingPaper());
            }}
    ),
    VALLEY_SCENTS(
            new ArrayList<Flower>() {{
                add(Flower.GERBERA);
                add(Flower.ROSE);
                add(Flower.LAVENDER);
                add(Flower.LILY);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new RedTape());
                add(new PinkPackingPaper());
                add(new TransparentPackagingWrap());
            }}
    ),
    SCENTED_DREAM(
            new ArrayList<Flower>() {{
                add(Flower.GAZANIA);
                add(Flower.GLADIOLUS);
                add(Flower.CHRYSANTHEMUM);
                add(Flower.LILY);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new BlackTape());
                add(new GoldTape());
                add(new TransparentPackagingWrap());
            }}
    ),
    JUST_FOR_YOU(
            new ArrayList<Flower>() {{
                add(Flower.HORTENSIA);
                add(Flower.NARCISSUS);
                add(Flower.CORNFLOWER);
                add(Flower.LAVENDER);
            }},
            new ArrayList<BouquetDecorationElement>() {{
                add(new GreenPackingPaper());
                add(new PinkPackingPaper());
                add(new WhitePackingPaper());
            }}
    );

    List<Flower> flowers;
    List<BouquetDecorationElement> decorationElements;

    BouquetEnum(List<Flower> flowers,
                List<BouquetDecorationElement> decorationElements) {
this.flowers = flowers;
this.decorationElements = decorationElements;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public List<BouquetDecorationElement> getDecorationElements() {
        return decorationElements;
    }
}
