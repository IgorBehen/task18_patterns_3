package epam.university.model.discount.impl;

import epam.university.model.discount.DiscountCardType;

public class GoldCard extends DiscountCardType {

  private final String TO_NAME = " + free delivery";


  public String getName() {
    return TO_NAME;
  }
}
