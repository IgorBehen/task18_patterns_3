package epam.university.model.bouquet.decoration.event;

import epam.university.model.bouquet.IBouquet;
import epam.university.model.bouquet.decoration.element.BlackTape;
import epam.university.model.bouquet.decoration.element.GoldTape;
import epam.university.model.bouquet.decoration.element.TransparentPackagingWrap;

public class Funeral implements Event{
    private BlackTape blackTapeDecoration;
    private GoldTape goldTapeDecoration;
    private TransparentPackagingWrap transparentPackagingWrap;

    public Funeral() {
        blackTapeDecoration = new BlackTape();
        goldTapeDecoration = new GoldTape();
        transparentPackagingWrap = new TransparentPackagingWrap();
    }

    public IBouquet decorate(IBouquet IBouquet) {
        blackTapeDecoration.setBouquet(IBouquet);
        goldTapeDecoration.setBouquet(blackTapeDecoration);
        transparentPackagingWrap.setBouquet(goldTapeDecoration);
        return transparentPackagingWrap;
    }
}
