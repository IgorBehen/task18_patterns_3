package epam.university;

import epam.university.controller.AppController;

public class App {

  public static void main(String[] args) {
    new AppController().start();
  }

}
