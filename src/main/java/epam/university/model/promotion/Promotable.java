package epam.university.model.promotion;

public interface Promotable {

  void add(PromotionSubscriber promotionSubscriber);

  void remove(PromotionSubscriber promotionSubscriber);

  void notifySubscribers();
}
