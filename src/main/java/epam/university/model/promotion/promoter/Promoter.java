package epam.university.model.promotion.promoter;

import epam.university.model.promotion.Promotable;
import epam.university.model.promotion.PromotionSubscriber;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class Promoter implements Promotable {

  private List<String> promotions;
  private final String promotionsSource = "src\\main\\resources\\promotions.properties";
  private String currentPromotion;
  private List<PromotionSubscriber> subscribers;

  private final Random random = new Random();

  public Promoter() {
    promotions = new LinkedList<>();
    initPromotions(promotionsSource);
    subscribers = new LinkedList<>();
  }

  private void initPromotions(String source) {
    try (InputStream input = new FileInputStream(promotionsSource)) {
      Properties prop = new Properties();
      prop.load(input);
      Integer i = 1;
      while (prop.containsKey(i.toString())) {
        promotions.add(prop.getProperty(i.toString()));
        i++;
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public String getPromotion() {
    return currentPromotion = promotions.get(random.nextInt(promotions.size()));
  }

  @Override
  public void add(PromotionSubscriber promotionSubscriber) {
    subscribers.add(promotionSubscriber);
  }

  @Override
  public void remove(PromotionSubscriber promotionSubscriber) {
    subscribers.remove(promotionSubscriber);
  }

  @Override
  public void notifySubscribers() {
    subscribers.forEach(s -> s.update(currentPromotion));
  }
}
