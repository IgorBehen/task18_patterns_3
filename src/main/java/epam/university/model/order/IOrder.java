package epam.university.model.order;

import epam.university.model.client.Client;

public interface IOrder {

  double getCost();

  String getName();

  Client getClient();

 // double getDeliveryPrice();

  //void setDeliveryPrice(double deliveryPrice);
}
