package epam.university.model.bouquet;


import java.util.List;

public interface IBouquet {

  double getPrice();

  List<Flower> getFlowers();

  List<BouquetDecorationElement> getBouquetDecorationElements();

}
