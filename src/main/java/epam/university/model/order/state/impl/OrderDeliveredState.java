package epam.university.model.order.state.impl;

import epam.university.model.order.state.State;

public class OrderDeliveredState implements State {

  @Override
  public void getOrderStateName() {
    System.out.println("DeliveredOrderState");
  }
}
