package epam.university.model.promotion;

public interface PromotionSubscriber {

  void update(Object promotion);
}
