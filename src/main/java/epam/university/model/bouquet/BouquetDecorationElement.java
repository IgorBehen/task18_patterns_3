package epam.university.model.bouquet;

import java.util.List;
import java.util.Optional;

public class BouquetDecorationElement implements IBouquet {
    private Optional<IBouquet> bouquet;
    private double usedDecorationElementCost;
    private BouquetDecorationElement usedDecorationElement;
    protected String DECORATION_NAME = "BouquetDecorationElement";

    public void setBouquet(IBouquet IBouquet){
        this.bouquet = Optional.ofNullable(IBouquet);
        if (usedDecorationElement !=null){
            this.bouquet.orElseThrow(IllegalArgumentException::new).getBouquetDecorationElements().add(usedDecorationElement);
        }
    }

    protected void setUsedDecorationElementCost(double usedDecorationElementCost){
        this.usedDecorationElementCost = usedDecorationElementCost;
    }

    public void setUsedDecorationElement(BouquetDecorationElement usedDecorationElement){
        this.usedDecorationElement = usedDecorationElement;
    }

    @Override
    public double getPrice(){
        return this.bouquet.orElseThrow(IllegalArgumentException::new).getPrice()+ usedDecorationElementCost;
    }

    @Override
    public List<Flower> getFlowers() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getFlowers();
    }

    public List<BouquetDecorationElement> getBouquetDecorationElements(){
        return bouquet.orElseThrow(IllegalArgumentException::new).getBouquetDecorationElements();
    }

    @Override
    public String toString() {
        return DECORATION_NAME;
    }
}
