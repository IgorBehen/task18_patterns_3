package epam.university.model.order.decorator.packaging;

public class SafePackaging extends Packaging {

  private final double PRICE = 25;
  private final String TO_NAME = " + SafePackaging";


  public SafePackaging(){
    setAdditionalPrice(PRICE);
    setToName(TO_NAME);
  }

}
