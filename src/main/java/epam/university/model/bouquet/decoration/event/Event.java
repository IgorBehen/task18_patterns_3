package epam.university.model.bouquet.decoration.event;

import epam.university.model.bouquet.IBouquet;

public interface Event {
    IBouquet decorate(IBouquet IBouquet);
}
