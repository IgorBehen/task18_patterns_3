package epam.university.model.bouquet;

public enum Flower {
    ROSE(15), LAVENDER(23), TULIP(18), GAZANIA(8), CORNFLOWER(5),
    CHRYSANTHEMUM(15), NARCISSUS(12), GERBERA(25), GLADIOLUS(16), HORTENSIA(25), LILY(10), PEONY(19);

   private int price;

    Flower(int i) {
       price = i;
    }

    public int getPrice() {
        return price;
    }
}
