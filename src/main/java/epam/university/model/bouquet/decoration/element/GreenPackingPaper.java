package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class GreenPackingPaper extends BouquetDecorationElement {

    private final double DECORATION_COST = 30;

    public GreenPackingPaper() {
        DECORATION_NAME = "Green Packing Paper";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
