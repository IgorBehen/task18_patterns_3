package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class PinkPackingPaper extends BouquetDecorationElement {
    private final double DECORATION_COST = 26;

    public PinkPackingPaper() {
        DECORATION_NAME = "Pink Packing Paper";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
