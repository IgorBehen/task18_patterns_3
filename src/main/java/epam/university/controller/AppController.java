package epam.university.controller;

import epam.university.model.client.Client;
import epam.university.model.client.SystemClient;
import epam.university.model.discount.impl.NewClientCard;
import epam.university.model.order.Order;
import epam.university.model.promotion.promoter.Promoter;
import epam.university.view.View;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class AppController {

  private Scanner scn;
  private String userInput;
  private View view;
  private Order order;

  private Promoter promoter;
  private Random random;
  private Thread promoterServer = new Thread(this::promotionServer);
  private boolean serverRun = true;

  public AppController() {
    scn = new Scanner(System.in);
    view = new View();
    order = new Order();
    promoter = new Promoter();
    random = new Random();
  }

  public void start() {
    initClient();
    promoter.add(order.getClient());
    //promoterServer.start();
    System.out.println("---Going to state Bouquet Creation---");
    order.bouquetCreation();
    if (order.getBouquet().getBouquetDecorationElements().size() == 0) {
      order.bouquetDecoration();
    }
    System.out.println("Created Bouquet: " + order.getBouquet());
    System.out.println("---Going to state Register Order---");
    order.registerOrder();
    System.out.println(order);
    serverRun = false;
  }

  private void initClient() {
    view.printMenu(Arrays.asList("New user", "Sign in"), "input");
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    if (userInput.equals("1")) {
      Client client = new Client();
      client.setDiscountCard(new NewClientCard());
      order.setClient(client);
    } else if (userInput.equals("2")) {
      handleSignIn();
    }
  }

  private void handleSignIn() {
    boolean userFound = false;
    SystemClient signedInClient = null;
    while (!userFound) {
      System.out.print("\nEnter your phone number: ");
      userInput = scn.nextLine();
      try {
        signedInClient = Arrays.stream(SystemClient.values())
            .filter(u -> userInput.equals(u.getPhoneNumber()))
            .findFirst().get();
      } catch (NoSuchElementException e) {
        System.err.println(e);
        continue;
      }
      System.out.println("\nWelcome!");
      userFound = true;
      }
    Client client = new Client();
    client.setDiscountCard(signedInClient.getDiscountCardType());
    order.setClient(client);
    }

  private void promotionServer() {
    while (serverRun) {
      promoter.getPromotion();
      promoter.notifySubscribers();
      int sleepTime = 12000;
      while (sleepTime > 11000) {
        sleepTime = random.nextInt(50000);
      }
      try {
        Thread.sleep(sleepTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

