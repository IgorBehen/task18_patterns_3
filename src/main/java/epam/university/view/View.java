package epam.university.view;

import epam.university.model.bouquet.BouquetDecorationElement;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import java.util.List;

public class View {

  public void printMenu(List<Object> menu, String prompt) {
    System.out.print("\n");
    int i = 1;
    for (Object o : menu) {
      System.out.print("\t[" + i + "] " + o + "\n");
      i++;
    }
    System.out.print("\t[q] Quit");
    System.out.print("\n" + prompt + ": ");
  }

  public void printBouquets(List<BouquetEnum> bouquets) {
    System.out.print("\n");
    int i = 1;
    for (BouquetEnum b : bouquets) {
      System.out.print("\t[" + i + "] " + b + "\n");
      i++;
    }
    System.out.print("\t[" + i + "] Create custom bouquet\n");
    System.out.print("\ninput: ");
  }

  public void printFlowers(List<Flower> flowers) {
    System.out.print("\n");
    int i = 1;
    for (Flower f : flowers) {
      System.out.print("\t[" + i + "] " + f + "\n");
      i++;
    }
    System.out.print("\t[" + i + "] Go to decoration\n");
    System.out.print("\ninput: ");
  }

  public void printDecorationElements(List<BouquetDecorationElement> decorationElements) {
    System.out.print("\n");
    int i = 1;
    for (BouquetDecorationElement b : decorationElements) {
      System.out.print("\t[" + i + "] " + b + "\n");
      i++;
    }
    System.out.print("\t[" + i + "] finish\n");
    System.out.print("\ninput: ");
  }

  public void printPromotion(Object promotion) {
    for (int i = 0; i < 35; i++) {
      System.out.print("\t");
    }
    System.out.println(promotion);
  }
}
