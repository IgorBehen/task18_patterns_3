package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class WhitePackingPaper extends BouquetDecorationElement {
    private final double DECORATION_COST = 32;

    public WhitePackingPaper() {
        DECORATION_NAME = "White Packing Paper";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
