package epam.university.model.discount.impl;

import epam.university.model.discount.DiscountCardType;

public class NewClientCard extends DiscountCardType {

  private final String TO_NAME = " + NewClientCard";

  public String getName() {
    return TO_NAME;
  }
}
