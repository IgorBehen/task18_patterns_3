package epam.university.model.bouquet.decoration.event;

import epam.university.model.bouquet.IBouquet;
import epam.university.model.bouquet.decoration.element.GoldTape;
import epam.university.model.bouquet.decoration.element.RedTape;
import epam.university.model.bouquet.decoration.element.PinkPackingPaper;
import epam.university.model.bouquet.decoration.element.WhitePackingPaper;

public class Wedding implements Event{
    private RedTape redTapeDecoration;
    private WhitePackingPaper whitePackingPaper;
    private PinkPackingPaper pinkPackingPaper;
    private GoldTape goldTapeDecoration;

    public Wedding() {
        redTapeDecoration = new RedTape();
        whitePackingPaper = new WhitePackingPaper();
        pinkPackingPaper = new PinkPackingPaper();
        goldTapeDecoration = new GoldTape();
    }

    public IBouquet decorate(IBouquet IBouquet) {
        redTapeDecoration.setBouquet(IBouquet);
        whitePackingPaper.setBouquet(redTapeDecoration);
        pinkPackingPaper.setBouquet(whitePackingPaper);
        goldTapeDecoration.setBouquet(pinkPackingPaper);
        return goldTapeDecoration;
    }
}
