package epam.university.model.discount;

import epam.university.model.order.decorator.OrderDecorator;
import epam.university.model.order.IOrder;

public class DiscountConverter extends OrderDecorator implements DiscountAmount {

  private DiscountCard discountCard;
  private double discountSum;
  private IOrder order;

  public DiscountConverter(DiscountCard discountCard, IOrder order){
  this.discountCard = discountCard;
  this.order = order;
  discountSum = getDiscount(order);
    setAdditionalPrice(-discountSum);
    setToName(discountCard.getName());
  }

  @Override
  public double getDiscount(IOrder order) {
    if (this.discountCard.getName().equals(" + RegularClientCard")) {
      discountSum = order.getCost() * (discountCard.getPercentage() / 100);
      return discountSum;
    }else if (this.discountCard.getName().equals(" + free delivery")){
      OrderDecorator orderDecorator = (OrderDecorator) order;
      return orderDecorator.getOrderDeliveryPrice();

    } else if (this.discountCard.getName().equals(" + used bonuses")){
      return discountCard.getBonuses();
    } else {
      return 0;
    }
  }

}
