package epam.university.model.client;

import epam.university.model.discount.DiscountCardType;
import epam.university.model.order.IOrder;
import epam.university.model.promotion.PromotionSubscriber;
import epam.university.view.View;

public class Client implements PromotionSubscriber {

  private DiscountCardType discountCard;
  private IOrder order;

  private View view;

  public Client() {
    view = new View();
  }

  public DiscountCardType getDiscountCard() {
    return discountCard;
  }

  public void setDiscountCard(DiscountCardType discountCard) {
    this.discountCard = discountCard;
  }

  @Override
  public void update(Object promotion) {
    view.printPromotion(promotion);
  }

  @Override
  public String toString() {
    return "Client{" +
        "discountCard=" + discountCard +
        ", view=" + view +
        '}';
  }
}
