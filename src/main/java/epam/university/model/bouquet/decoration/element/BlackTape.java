package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class BlackTape extends BouquetDecorationElement {
    private final double DECORATION_COST = 15;

    public BlackTape() {
        DECORATION_NAME = "Black Tape";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
