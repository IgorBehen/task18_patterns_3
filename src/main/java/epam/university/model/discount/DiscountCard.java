package epam.university.model.discount;

public interface DiscountCard {

  double getPercentage();

  int getBonuses();

  String getName();

}
