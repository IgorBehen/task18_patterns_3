package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class DefaultTape extends BouquetDecorationElement {
    private final double DECORATION_COST = 0;

    public DefaultTape() {
        DECORATION_NAME = "DefaultTape";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
